<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    
    $sheep = new Animal("shaun");

    echo "Nama Binatang : " .$sheep->name. "<br>"; // "shaun"
    echo "Jumlah Kaki : " .$sheep->legs. "<br>"; // 4
    echo "Berdarah dingin : " .$sheep->cold_blooded. "<br><br>"; // "no"

    $kera = new Ape("Kera Sakti");

    echo "Nama Binatang : " .$kera->name. "<br>"; // "shaun"
    echo "Jumlah Kaki : " .$kera->legs. "<br>"; // 4
    echo "Berdarah dingin : " .$kera->cold_blooded. "<br>"; // "no"
    echo $kera->yell();

    $kodok = new Frog("buduk");

    echo "Nama Binatang : " .$kodok->name. "<br>"; // "shaun"
    echo "Jumlah Kaki : " .$kodok->legs. "<br>"; // 4
    echo "Berdarah dingin : " .$kodok->cold_blooded. "<br>"; // "no"
    echo $kodok->jump();

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())





?>